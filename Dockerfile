FROM ubuntu:20.04

ARG DEBIAN_FRONTEND=noninteractive
ARG RUNUSER=runner
ARG RUNGROUP=runner

ARG ACTIONS_VERSION="2.292.0"

COPY build.sh /tmp

RUN apt-get update \
  && apt-get install -y --no-install-recommends \
    ca-certificates \
    curl \
    git \
    gnupg \
    gpg \
    libfontconfig1 \
    libfreetype6 \
    ssh-client \
    tini \
    ssh-client \
    python3 \
    unzip \
    pip \
    docker \
    python3-pip \
    wget  \
    tar \
    vim \ 
  && rm -rf /var/lib/apt/lists/*


################################
# Install Ansible
################################
RUN pip3 install --upgrade pip && \
    pip3 install --upgrade virtualenv && \
    pip3 install pywinrm && \
    pip3 install ansible

RUN /tmp/build.sh

COPY --chown=${RUNUSER}:${RUNGROUP} entrypoint.sh /home/${RUNUSER}

RUN rm -rf /tmp/*

WORKDIR /home/${RUNUSER}
USER ${RUNUSER}

ENTRYPOINT ["./entrypoint.sh"]
